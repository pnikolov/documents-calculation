### Prerequisites

* JDK 17 (can be downloaded from [Eclipse Adoptium](https://adoptium.net/)).

### Running

* Execute `.\gradlew bootRun` from the root project directory to run the Spring Boot application.
* Swagger UI is accessible at http://localhost:8080/swagger-ui/index.html.

### The task

We have a **CSV** file, containing a list of invoices, debit and credit notes in different
currencies. **Document structure** with **demo data** can be found in the [`data.csv` file](./data.csv).

API endpoint should allow you to pass:
- CSV file
- A list of currencies and exchange rates (for example: `EUR:1,USD:0.987,GBP:0.878`)
- An output currency (for example: `GBP`)
- Filter by a specific customer by VAT number (as an optional input)

Keep in mind that the exchange rates are always based on the default currency.  
The default currency is specified by giving it an exchange rate of 1. EUR is used as a default currency only for the example.  
For example:
```
EUR = 1  
EUR:USD = 0.987  
EUR:GBP = 0.878
```

The response should contain **the sum of all documents per customer**. If the optional input filter is used, the functionality should **return only the sum of the
invoices for that specific customer**.

Invoice types:
- 1 = invoice
- 2 = credit note
- 3 = debit note

Note, that if we have a credit note, it should subtract from the total of the invoice and if we have a debit note, it should add to the sum of the invoice.
