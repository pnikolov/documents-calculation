package com.example.controller;

import static org.apache.commons.lang3.StringUtils.isNotBlank;

import com.example.contract.CalculateRequest;
import com.example.contract.CalculateResponse;
import com.example.exception.DocumentException;
import com.example.exception.ExchangeRatException;
import com.example.service.InvoiceCalculator;
import java.io.IOException;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RequiredArgsConstructor
@RestController
@RequestMapping(value = "/api/v1", produces = MediaType.APPLICATION_JSON_VALUE)
public class InvoiceController {
  private final InvoiceCalculator calculator;

  @PostMapping(value = "/sumInvoices", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
  ResponseEntity<CalculateResponse> sumInvoices(@Valid CalculateRequest request)
      throws IOException {
    try {
      var response = calculator.calculateBalance(request);

      return isNotBlank(request.getCustomerVat()) && response.customers().isEmpty()
          ? ResponseEntity.notFound().build()
          : ResponseEntity.ok(response);
    } catch (DocumentException | ExchangeRatException exception) {
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage(), exception);
    }
  }
}
