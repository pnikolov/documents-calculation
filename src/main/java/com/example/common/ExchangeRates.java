package com.example.common;

import static java.lang.Double.parseDouble;
import static java.text.MessageFormat.format;

import com.example.exception.ExchangeRatException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.apache.commons.lang3.math.NumberUtils;

public class ExchangeRates {
  private static final String SEPARATOR = ":";
  private static final int CURRENCY_CODE_INDEX = 0;
  private static final int RATE_INDEX = 1;

  private final Map<String, Double> rates = new HashMap<>();

  public ExchangeRates(List<String> source) {

    source.forEach(
        value -> {
          var parts = value.trim().split(SEPARATOR);

          if (parts.length != 2 || !NumberUtils.isParsable(parts[RATE_INDEX])) {
            throw new ExchangeRatException(format("Invalid exchange rate format: {0}.", value));
          }

          rates.put(parts[CURRENCY_CODE_INDEX], parseDouble(parts[RATE_INDEX]));
        });
  }

  public double convert(double amount, String from, String to) {
    return amount * getRate(from, to);
  }

  public double getRate(String from, String to) {
    return getRate(from) / getRate(to);
  }

  public double getRate(String currency) {
    return Optional.ofNullable(rates.get(currency))
        .orElseThrow(
            () -> new ExchangeRatException(format("{0} exchange rate not found.", currency)));
  }
}
