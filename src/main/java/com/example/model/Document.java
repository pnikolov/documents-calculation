package com.example.model;

import com.opencsv.bean.CsvBindByName;
import lombok.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Document {
  @CsvBindByName(column = "Customer", required = true)
  private String customer;

  @CsvBindByName(column = "Vat number", required = true)
  private String vatNumber;

  @CsvBindByName(column = "Document number", required = true)
  private String documentNumber;

  @CsvBindByName(column = "Type", required = true)
  private int type;

  @CsvBindByName(column = "Parent document")
  private String parentDocument;

  @CsvBindByName(required = true)
  private String currency;

  @CsvBindByName(required = true)
  private double total;

  @AllArgsConstructor
  public enum Type {
    INVOICE(1),
    CREDIT_NOTE(2),
    DEBIT_NOTE(3);

    @Getter private final int value;
  }
}
