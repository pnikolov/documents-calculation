package com.example.exception;

public class ExchangeRatException extends RuntimeException {
  public ExchangeRatException(String message) {
    super(message);
  }
}
