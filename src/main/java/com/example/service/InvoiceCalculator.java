package com.example.service;

import com.example.contract.CalculateRequest;
import com.example.contract.CalculateResponse;
import java.io.IOException;

public interface InvoiceCalculator {
  CalculateResponse calculateBalance(CalculateRequest request) throws IOException;
}
