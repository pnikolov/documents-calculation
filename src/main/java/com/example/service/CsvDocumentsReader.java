package com.example.service;

import static java.text.MessageFormat.format;
import static java.util.stream.Collectors.toSet;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

import com.example.exception.DocumentException;
import com.example.model.Document;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.enums.CSVReaderNullFieldIndicator;
import com.opencsv.exceptions.CsvException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class CsvDocumentsReader implements DocumentsReader {
  @Override
  public List<Document> read(InputStream stream) throws IOException {
    try (var reader = new InputStreamReader(stream)) {
      var documents =
          new CsvToBeanBuilder<Document>(reader)
              .withType(Document.class)
              .withFieldAsNull(CSVReaderNullFieldIndicator.EMPTY_SEPARATORS)
              .build()
              .parse();

      var documentIds = documents.stream().map(Document::getDocumentNumber).collect(toSet());

      documents.forEach(
          document -> {
            if (isNotBlank(document.getParentDocument())
                && !documentIds.contains(document.getParentDocument())) {
              throw new DocumentException(
                  format("Parent document {0} not found.", document.getParentDocument()));
            }
          });

      return documents;
    } catch (RuntimeException exception) {
      if (exception.getCause() instanceof CsvException csvException) {
        throw new DocumentException(csvException.getMessage(), csvException);
      }
      throw exception;
    }
  }
}
