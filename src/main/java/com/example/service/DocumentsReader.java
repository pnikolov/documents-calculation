package com.example.service;

import com.example.model.Document;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public interface DocumentsReader {
  List<Document> read(InputStream stream) throws IOException;
}
