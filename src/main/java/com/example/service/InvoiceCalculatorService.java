package com.example.service;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.reducing;
import static org.apache.commons.lang3.StringUtils.isBlank;

import com.example.common.ExchangeRates;
import com.example.contract.CalculateRequest;
import com.example.contract.CalculateResponse;
import com.example.contract.Customer;
import com.example.model.Document;
import java.io.IOException;
import java.util.function.Function;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class InvoiceCalculatorService implements InvoiceCalculator {
  private final DocumentsReader documentsReader;

  @Override
  public CalculateResponse calculateBalance(CalculateRequest request) throws IOException {
    var documents = documentsReader.read(request.getFile().getInputStream());
    var exchangeRates = new ExchangeRates(request.getExchangeRates());

    Function<Document, Double> processDocument =
        document -> {
          var total =
              exchangeRates.convert(
                  document.getTotal(), document.getCurrency(), request.getOutputCurrency());
          return document.getType() == Document.Type.CREDIT_NOTE.getValue() ? -total : total;
        };

    var customers =
        documents.stream()
            .filter(
                document ->
                    isBlank(request.getCustomerVat())
                        || request.getCustomerVat().equalsIgnoreCase(document.getVatNumber()))
            .collect(groupingBy(Document::getCustomer, reducing(0d, processDocument, Double::sum)))
            .entrySet()
            .stream()
            .map(entry -> new Customer(entry.getKey(), entry.getValue()))
            .toList();

    return new CalculateResponse(request.getOutputCurrency(), customers);
  }
}
