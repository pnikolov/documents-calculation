package com.example.contract;

import java.util.List;

public record CalculateResponse(String currency, List<Customer> customers) {}
