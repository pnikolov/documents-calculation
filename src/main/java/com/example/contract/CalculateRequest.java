package com.example.contract;

import java.util.List;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import lombok.Builder;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
@Builder
public class CalculateRequest {
  @NotNull private MultipartFile file;

  @NotEmpty private List<@Pattern(regexp = "^(\\w){3}:\\d*(.\\d+)*$") String> exchangeRates;

  @NotEmpty
  @Pattern(regexp = "^(\\w){3}$")
  private String outputCurrency;

  private String customerVat;
}
