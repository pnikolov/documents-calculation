package com.example.contract;

public record Customer(String name, double balance) {}
