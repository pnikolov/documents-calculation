package com.example.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import com.example.contract.CalculateRequest;
import com.example.contract.Customer;
import com.example.model.Document;
import java.io.IOException;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

@SpringBootTest
class InvoiceCalculatorServiceTest {
  private static final List<Document> DOCUMENTS =
      List.of(
          Document.builder()
              .customer("customer1")
              .vatNumber("123")
              .documentNumber("100")
              .type(Document.Type.INVOICE.getValue())
              .total(100)
              .currency("USD")
              .build(),
          Document.builder()
              .customer("customer1")
              .vatNumber("123")
              .documentNumber("101")
              .type(Document.Type.DEBIT_NOTE.getValue())
              .total(50)
              .currency("USD")
              .build(),
          Document.builder()
              .customer("customer2")
              .vatNumber("456")
              .documentNumber("200")
              .type(Document.Type.INVOICE.getValue())
              .total(200)
              .currency("BGN")
              .build(),
          Document.builder()
              .customer("customer2")
              .vatNumber("456")
              .documentNumber("201")
              .type(Document.Type.CREDIT_NOTE.getValue())
              .total(100)
              .currency("BGN")
              .build());

  private static final List<String> EXCHANGE_RATES = List.of("USD:1", "BGN:0.5");

  private static final MultipartFile DUMMY_FILE = new MockMultipartFile("file", new byte[0]);

  @MockBean private DocumentsReader documentsReader;
  @Autowired private InvoiceCalculatorService calculatorService;

  @Test
  void calculateBalance_NoCustomerFilter() throws IOException {
    when(documentsReader.read(any())).thenReturn(DOCUMENTS);

    var result =
        calculatorService.calculateBalance(
            CalculateRequest.builder()
                .file(DUMMY_FILE)
                .exchangeRates(EXCHANGE_RATES)
                .outputCurrency("USD")
                .build());

    assertThat(result.currency()).isEqualTo("USD");
    assertThat(result.customers())
        .hasSize(2)
        .contains(new Customer("customer1", 150), new Customer("customer2", 50));
  }

  @Test
  void calculateBalance_CustomerFilter() throws IOException {
    when(documentsReader.read(any())).thenReturn(DOCUMENTS);

    var result =
        calculatorService.calculateBalance(
            CalculateRequest.builder()
                .file(DUMMY_FILE)
                .exchangeRates(EXCHANGE_RATES)
                .outputCurrency("BGN")
                .customerVat("456")
                .build());

    assertThat(result.currency()).isEqualTo("BGN");
    assertThat(result.customers()).containsExactly(new Customer("customer2", 100));
  }
}
