package com.example.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import com.example.exception.DocumentException;
import com.example.model.Document;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class CsvDocumentsReaderTest {
  @Autowired private CsvDocumentsReader documentsReader;

  @Test
  public void read_Successful() throws IOException {
    final String input =
        """
        Customer,Vat number,Document number,Type,Parent document,Currency,Total
        Vendor 1,123456789,1000000257,1,,USD,400
        Vendor 2,987654321,1000000258,1,,EUR,900
        """;

    var documents = documentsReader.read(new ByteArrayInputStream(input.getBytes()));

    assertThat(documents)
        .hasSize(2)
        .contains(
            Document.builder()
                .customer("Vendor 1")
                .vatNumber("123456789")
                .documentNumber("1000000257")
                .type(1)
                .currency("USD")
                .total(400)
                .build(),
            Document.builder()
                .customer("Vendor 2")
                .vatNumber("987654321")
                .documentNumber("1000000258")
                .type(1)
                .currency("EUR")
                .total(900)
                .build());
  }

  @ParameterizedTest
  @MethodSource
  public void read_InvalidInput(String input) {
    assertThrows(
        DocumentException.class,
        () -> documentsReader.read(new ByteArrayInputStream(input.getBytes())));
  }

  private static Stream<String> read_InvalidInput() {
    return Stream.of(
        """
        Customer,Vat number,Document number,Type,Parent document,Currency,Total
        ,123456789,1000000257,1,,USD,400
        Vendor 2,987654321,1000000258,,,EUR,900
        """,
        """
        Customer,Vat number,Document number,Type,Parent document,Currency,Total
        Vendor 1,123456789,1000000257,1,,USD,400
        Vendor 2,987654321,1000000258,1,123,EUR,900
        """);
  }
}
