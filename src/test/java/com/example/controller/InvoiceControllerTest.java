package com.example.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.example.contract.CalculateResponse;
import com.example.contract.Customer;
import com.example.exception.DocumentException;
import com.example.exception.ExchangeRatException;
import com.example.service.InvoiceCalculator;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(InvoiceController.class)
public class InvoiceControllerTest {
  private static final MockMultipartFile DUMMY_FILE = new MockMultipartFile("file", new byte[0]);

  @Autowired private MockMvc mockMvc;

  @MockBean private InvoiceCalculator calculator;

  @Test
  public void sumInvoices_Successful() throws Exception {
    var response = new CalculateResponse("EUR", List.of(new Customer("customer", 100)));

    when(calculator.calculateBalance(any())).thenReturn(response);

    mockMvc
        .perform(
            multipart("/api/v1/sumInvoices")
                .file(DUMMY_FILE)
                .param("exchangeRates", "EUR:1")
                .param("outputCurrency", "EUR"))
        .andExpect(status().isOk())
        .andExpect(content().json(new ObjectMapper().writeValueAsString(response)));
  }

  @ParameterizedTest
  @MethodSource
  public void sumInvoices_CalculatorException(
      Class<? extends Exception> exceptionClass, HttpStatus status) throws Exception {
    when(calculator.calculateBalance(any())).thenThrow(exceptionClass);

    mockMvc
        .perform(
            multipart("/api/v1/sumInvoices")
                .file(DUMMY_FILE)
                .param("exchangeRates", "EUR:1")
                .param("outputCurrency", "EUR"))
        .andExpect(status().is(status.value()));
  }

  @ParameterizedTest
  @MethodSource
  public void sumInvoices_InvalidInput(String exchangeRates, String outputCurrency)
      throws Exception {
    mockMvc
        .perform(
            multipart("/api/v1/sumInvoices")
                .file(DUMMY_FILE)
                .param("exchangeRates", exchangeRates)
                .param("outputCurrency", outputCurrency))
        .andExpect(status().isBadRequest());
  }

  private static Stream<Arguments> sumInvoices_CalculatorException() {
    return Stream.of(
        Arguments.of(ExchangeRatException.class, HttpStatus.BAD_REQUEST),
        Arguments.of(DocumentException.class, HttpStatus.BAD_REQUEST));
  }

  private static Stream<Arguments> sumInvoices_InvalidInput() {
    return Stream.of(
        Arguments.of(null, "EUR"),
        Arguments.of("", "USD"),
        Arguments.of("EUR:0.5", ""),
        Arguments.of("ABCD:1", "USD"),
        Arguments.of("EUR:1", "ABCD"));
  }
}
